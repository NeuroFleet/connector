# Description:
#   Random jokes from /r/jokes
#
# Dependencies:
#   "soupselect: "0.2.0"
# 
# Configuration:
#   None
#
# Commands:
#   hubot reddit (me) <reddit> [limit] - Lookup reddit topic
#
#   hubot joke me - Pull a random joke from /r/jokes
#   hubot joke me <list> - Pull a random joke from /r/<list>
#
#   hubot joke me dad - pulls a random dad joke
#   hubot joke me mom - pulls a random momma joke
#   hubot joke me clean - pulls a random clean joke
#   hubot joke me classy - pulls a random classic joke
#
#   hubot a reddit <subreddit> - A random top (today) post from the specified subreddit. Tries to find a picture if possible
#
# Author:
#   EnriqueVidal

Select      = require( "soupselect" ).select

lookup_site = "http://www.reddit.com/"

module.exports = (robot)->
  robot.respond /reddit( me)? ([a-z0-9\-_\.]+\/?[a-z0-9\-_\.]+)( [0-9]+)?/i, (message)->
    lookup_reddit message, (text)->
      message.send text

  lookup_reddit = (message, response_handler)->
    top     = parseInt message.match[3]
    reddit  = "r/" + message.match[2] + ".json"

    location  = lookup_site + reddit

    message.http( location ).get() (error, response, body)->
      return response_handler "Sorry, something went wrong"                      if error
      return response_handler "Reddit doesn't know what you're talking about"    if response.statusCode == 404
      return response_handler "Reddit doesn't want anyone to go there any more." if response.statusCode == 403

      list  = JSON.parse( body ).data.children
      count = 0

      for item in list
        count++

        text = ( item.data.title || item.data.link_title ) + " - " + ( item.data.url || item.data.body )
        response_handler text

        break if count == top

  robot.respond /joke me(.*)$/i, (msg) ->
    name = msg.match[1].trim()

    if name is "dad"
      url = "dadjokes"
    else if name is "clean"
      url = "cleanjokes"
    else if name is "mom"
      url = "mommajokes"
    else if name is "classy"
      url = "classyjokes"
    else
      url = "jokes"

    msg.http("http://www.reddit.com/r/#{url}.json")
    .get() (err, res, body) ->
      try
        data = JSON.parse body
        children = data.data.children
        joke = msg.random(children).data

        joketext = joke.title.replace(/\*\.\.\.$/,'') + ' ' + joke.selftext.replace(/^\.\.\.\s*/, '')

        msg.send joketext.trim()

      catch ex
        msg.send "Erm, something went EXTREMELY wrong - #{ex}"




  robot.respond /a reddit( .+)*/i, (msg) ->
    reddit msg, msg.match[1]?.trim()


reddit = (msg, subreddit) ->
  url = if subreddit? then "http://www.reddit.com/r/#{subreddit}/top.json" else "http://www.reddit.com/top.json"
  msg
    .http(url)
      .get() (err, res, body) ->
        
        # Sometimes when a subreddit doesn't exist, it wants to redirect you to the search page.
        # Oh, and it doesn't send back 302s as JSON
        if body?.match(/^302/)?[0] == '302'
          msg.send "That subreddit does not seem to exist."
          return

        posts = JSON.parse(body)

        # If the response has an error attribute, let's get out of here.
        if posts.error?
          msg.send "That doesn't seem to be a valid subreddit. [http response #{posts.error}]"
          return

        unless posts.data?.children? && posts.data.children.length > 0
          msg.send "While that subreddit exists, there does not seem to be anything there."
          return

        post = getPost(posts)

        tries_to_find_picture = 0

        while post?.domain != "i.imgur.com" && tries_to_find_picture < 30
          post = getPost(posts)
          tries_to_find_picture++
        
        # Send pictures with the url on one line so Campfire displays it as an image
        if post.domain == 'i.imgur.com'
          msg.send "#{post.title} - http://www.reddit.com#{post.permalink}"
          msg.send post.url
        else
          msg.send "#{post.title} - #{post.url} - http://www.reddit.com#{post.permalink}"

getPost = (posts) ->
  random = Math.round(Math.random() * posts.data.children.length)
  posts.data.children[random]?.data
