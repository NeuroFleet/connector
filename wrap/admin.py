from django.contrib import admin

from .models import *

from nucleon.console.admin import PgpInline, SshInline
from nucleon.explor.admin import MegaInline, IpfsInline
from nucleon.linked.admin import LdfInline, LdpInline

################################################################################

class PhoneInline(admin.TabularInline):
    model = IdentityPhone

#*******************************************************************************

class EmailInline(admin.TabularInline):
    model = IdentityEmail

################################################################################

class IdentityAdmin(admin.ModelAdmin):
    list_display  = ('username','email','first_name','last_name')
    list_filter   = ('groups__name',)
    list_editable = ('username','email','first_name','last_name')

    inlines = (
        PhoneInline, EmailInline,
        SshInline, PgpInline,
        MegaInline, IpfsInline,
        LdfInline, LdpInline,
    )

