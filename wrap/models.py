from reactor.webapp.shortcuts import *

################################################################################

@Reactor.orm.register_model('identity')
class Identity(AbstractBaseUser):
    pass

#*******************************************************************************

@Reactor.orm.register_model('web_profile')
class WebProfile(models.Model):
    identity = models.ForeignKey(Identity, related_name='profiles', on_delete=models.CASCADE)

################################################################################

#class IdentityAuth(AbstractUserSocialAuth):
#    user = models.ForeignKey(Identity, related_name='custom_social_auth', on_delete=models.CASCADE)

#*******************************************************************************

#class SocialStorage(DjangoStorage):
#    user = IdentityAuth

################################################################################

@Reactor.orm.register_model('identity_phone')
class IdentityPhone(models.Model):
    owner     = models.ForeignKey(Identity, related_name='phones')
    alias     = models.CharField(max_length=48, blank=True)

    number    = models.CharField(max_length=64)

#*******************************************************************************

@Reactor.orm.register_model('identity_email')
class IdentityEmail(models.Model):
    owner     = models.ForeignKey(Identity, related_name='emails')
    alias     = models.CharField(max_length=48, blank=True)

    address   = models.EmailField()

    server    = models.CharField(max_length=128, blank=True)
    port      = models.IntegerField(default=495)
    password  = models.CharField(max_length=128, blank=True)
    start_tls = models.BooleanField(default=False)

